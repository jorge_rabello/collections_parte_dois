# lista
usuarios_data_science = [15, 23, 43, 56]
usuarios_machine_learning = [13, 23, 56, 42]

assistiram_os_dois_cursos = usuarios_data_science.copy()
assistiram_os_dois_cursos.extend(usuarios_machine_learning)

# uma lista repete elementos
print(assistiram_os_dois_cursos)
print(len(assistiram_os_dois_cursos))

# um set não repete
print(set(assistiram_os_dois_cursos))

#conjunto = set([1, 2, 3, 1]) | obs: conjuntos não possuem indexação
conjunto = {1, 2, 3, 1}
print(conjunto)

for usuario in set(assistiram_os_dois_cursos):
    print(usuario)

alunos_matematica = {15, 23, 43, 56}
alunos_portugues = {13, 23, 56, 42}

print(alunos_portugues | alunos_matematica)

print(alunos_portugues & alunos_matematica)

print(alunos_matematica - alunos_portugues)

print(15 in alunos_matematica - alunos_portugues)
print(23 in alunos_matematica - alunos_portugues)

print(alunos_matematica ^ alunos_portugues)

usuarios = {1, 5, 76, 34, 52, 13, 17}
print(len(usuarios))

usuarios.add(13)
print(len(usuarios))

usuarios.add(134)
print(len(usuarios))

usuarios = frozenset(usuarios)

meu_texto = "Bem-vindo meu nome é Jorge eu gosto muito de nomes e tenho meu cachorro e gosto muito de cachorro"
print(meu_texto.split())

print(set(meu_texto.split()))
