from collections import defaultdict
from collections import Counter

meu_texto = "Bem-vindo meu nome é Jorge eu gosto muito de nomes e tenho meu cachorro e gosto muito de cachorro"

print(set(meu_texto.split()))

aparicoes = {
    "Guilherme": 1,
    "cachorro": 2,
    "nome": 3,
    "vindo": 4,
}
print(aparicoes)
print(aparicoes["Guilherme"])
print(aparicoes.get("XTO", 0))
print(aparicoes.get("cachorro", 0))

dicionario = dict(Guilherme=2, cachorro=1)
print(dicionario)

aparicoes["Carlos"] = 12
print(aparicoes["Carlos"])

aparicoes["Carlos"] = 13
print(aparicoes["Carlos"])

del (aparicoes["Carlos"])
print(aparicoes)

print("cachorro" in aparicoes)
print("Carlos" in aparicoes)

for elemento in aparicoes:
    print(elemento)

for elemento in aparicoes.values():
    print(elemento)

print(1 in aparicoes.values())

for elemento in aparicoes.keys():
    valor = aparicoes[elemento]
    print(elemento, valor)

for elemento in aparicoes.items():
    print(elemento)

for chave, valor in aparicoes.items():
    print(chave, " = ", valor)

print(["palavra {}".format(chave) for chave in aparicoes.keys()])

meu_texto = meu_texto.lower()

print(meu_texto.split())

aparicoes = {}

for palavra in meu_texto.split():
    ate_agora = aparicoes.get(palavra, 0)
    aparicoes[palavra] = ate_agora + 1

print(aparicoes)

aparicoes = defaultdict(int)

for palavra in meu_texto.split():
    aparicoes[palavra] += 1

print("APARIÇÕES")
print(aparicoes)


class Conta:
    def __init__(self):
        print("Criando uma conta")


contas = defaultdict(Conta)


print(contas[15])
print(contas[17])


aparicoes = Counter()
for palavra in meu_texto.split():
    aparicoes[palavra] += 1

print("COUNTER")
print(aparicoes)


aparicoes = Counter(meu_texto.split())

print("TEXTO COM COUNTER")
print(aparicoes)
